/* PMJ BS Bingo
 * Copyright (C) 2021 PMJ Rocks (https://pmj.rocks)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var cells = rows * cols;

var bullshits = words.split("\n")
  .filter(word => word.length > 0)
  .map(word => word.trim());

var randomizedWords = randomize(bullshits);
var counter=1;
while (counter <= cells) {
  var field = document.getElementById('field' + counter++);
  if (!field.classList.contains('joker')) {
    field.getElementsByTagName('p')[0].innerHTML = randomizedWords.pop();
  }
}

function randomize(words) {
	var randomizedWords = [];
	var tempWords = words.slice();
	
	for (var i=0; i<cells-1; i++) {
		var wordCount = tempWords.length;
		var randomIndex = Math.floor(Math.random() * wordCount);
		var randomWord = tempWords[randomIndex];
		randomizedWords.push(randomWord);
		tempWords.splice(randomIndex,1);

		if (tempWords.length==0) {
			tempWords = words.slice();
		}

	}

	return randomizedWords;
}


// the game
var fields  = document.getElementsByClassName('card');

for (i=0;i<fields.length;i++)
{
  fields[i].onclick = function click(e) {
    // default states
    checked = document.getElementsByClassName('checked');
    for (list=0;list<checked.length;list++)
    {
      checked[list].classList.add('bg-primary');
      checked[list].classList.remove('bg-success');
    }
    var joker = document.getElementsByClassName('joker')[0];
    joker.classList.add('bg-primary');
    joker.classList.remove('bg-success');
    // change state
    if (this.classList.contains("unchecked"))
    {
      this.classList.remove("unchecked");
      this.classList.add("checked");
      this.classList.add("bg-primary");
    }
    else if (this.classList.contains("checked"))
    {
      this.classList.remove("checked");
      this.classList.remove("bg-primary");
      this.classList.remove("bg-success");
      this.classList.add("unchecked");
    }
    var winner_row  = check_rows();
    var winner_col  = check_cols();
    if (typeof winner_row !== 'undefined')
    {
      winner_row.forEach(function(elm) {
        var winner = document.getElementById('field'+elm);
        winner.classList.remove("bg-primary");
        winner.classList.add('bg-success');
      });
    }
    if (typeof winner_col !== 'undefined')
    {
      winner_col.forEach(function(elm) {
        var winner = document.getElementById('field'+elm);
        winner.classList.remove("bg-primary");
        winner.classList.add('bg-success');
      });
    }
  };
}

function check_rows()
{
  for (r=1;r<=rows;r++)
  {
    var row_check = true;
    var col_end = r*cols;
    var col_start = col_end - cols + 1;
    var fields_arr  = [];
    for (c=col_start;c<=col_end;c++)
    {
      var field = document.getElementById('field'+c);
      if (!field.classList.contains('checked') && !field.classList.contains('joker'))
      {
        row_check = false;
      }
      fields_arr.push(c);
    }
    if (row_check)
    {
      return fields_arr;
    }
  }
}

function check_cols()
{
  for (c=1;c<=cols;c++)
  {
    var col_check = true;
    var row_start = c;
    var row_end = c + (cols - 1) * rows;
    var fields_arr  = [];
    for (r=row_start;r<=row_end;r+=parseInt(cols))
    {
      var field = document.getElementById('field'+r);
      if (!field.classList.contains('checked') && !field.classList.contains('joker'))
      {
        col_check = false;
      }
      fields_arr.push(r);
    }
    if (col_check)
    {
      return fields_arr;
    }
  }
}
