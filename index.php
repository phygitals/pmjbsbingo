<?php
/* PMJ BS Bingo
 * Copyright (C) 2021 PMJ Rocks (https://pmj.rocks)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// config
$title  = "PPS Bullshit Bingo";
$subtitle = "zum Thema #PMTnein";
$cols = 5;
$rows = 5;
$joker = true;
$jokerrandom = false;
$jokertext  = "ORWELL'S JOKER";
$logo = "img/ppLogo-st-de-rgb-h90px.png";
$logolink = "https://www.piratenpartei.ch";

/****************************************/
/* DON'T EDIT BELOW THIS LINE           */
/* (unless you know what you are doing) */
/****************************************/

// calculate center
$jokerfield = floor($rows / 2) * $cols + ceil($cols / 2);
$jokerfield = ($cols % 2 || $rows % 2) && $cols > 2 && $rows > 2 ? $jokerfield : ceil($cols * $rows / 2);
$jokerfield = $jokerrandom ? random_int (1,$cols * $rows) : $jokerfield;
?>
<!DOCTYPE html>
<html lang="de">
  <head>
    <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/pmjbsbingo.css">
    <script type="text/javascript" src="js/words.js"></script>
  </head>
  
  <body>
  
    <div class="container py-2">
      <?php if (!empty($logo)) { ?>
      <a href="<?php echo $logolink; ?>" target="_blank"><img src="<?php echo $logo; ?>" /></a>
      <?php } ?>
      <h1 class="text-primary text-center"><?php echo $title; ?></h1>
      <?php if (!empty($subtitle)) { ?>
      <h3 class="text-center bg-primary py-2"><?php echo $subtitle; ?></h3>
      <?php } ?>
    </div>
    
    <div id="PMJBSBingo" class="container"><div class="border border-primary">
      <div class="row row-cols-<?php echo $cols; ?> row-eq-height no-gutters">
      <?php for ($i = 1; $i <= ($cols * $rows); $i++) { ?>
        <?php if ( $joker && $i == $jokerfield ) { ?>
        <div class="col">
          <div id="field<?php echo $i; ?>" class="card h-100 border-primary bg-primary rounded-0 joker">
            <div class="card-body text-center">
              <p class="justify-content-center align-self-center" style="hyphens: auto;">
              <?php echo $jokertext; ?>
              </p>
            </div>
          </div>
        </div>
        <?php } else { ?>
        <div class="col">
          <div id="field<?php echo $i; ?>" class="card h-100 border-primary rounded-0 unchecked">
            <div class="card-body text-center">
              <p class="justify-content-center align-self-center" style="hyphens: auto;">
              </p>
            </div>
          </div>
        </div>
        <?php } ?>
      <?php } ?>
      </div>
    </div></div>
    <div class="container text-right">
      <small>&copy; <?php echo date('Y'); ?> <a href="https://pmj.rocks" target="_blank">CyberPMJ</a> | Source: <a href="https://gitlab.com/pmjfun/pmjbsbingo" target="_blank">https://gitlab.com/pmjfun/pmjbsbingo</a></small>
    </div>
<script type="text/javascript">
var cols  = "<?php echo $cols; ?>";
var rows  = "<?php echo $rows; ?>";
</script>
<script type="text/javascript" src="js/pmjbsbingo.js"></script>
</body></html>